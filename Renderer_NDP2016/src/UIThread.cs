﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

using Simulation_NDP2016.Data;

namespace Renderer_NDP2016
{
	internal class UIThread
	{
        private Thread mThread = null;
        private Application mApplication = null;
		private UI.RendererWindow mWindow      = null;
		private Renderer          mParent      = null;
		private SimulationOutput  mSimOutput   = null;

		public UIThread(Renderer _parent)
		{
			mParent = _parent;

			mThread = new Thread(new ThreadStart(this.ThreadedRun));
			mThread.SetApartmentState(ApartmentState.STA);
		}

        public void StartUIThread(SimulationOutput _simOutput)
		{
			mSimOutput = _simOutput;
			mThread.Start();
		}

		public void ExplicitShutdown()
		{
			if (mApplication != null)
				mApplication.Shutdown();
		}

		public void Join()
		{
			mThread.Join();
		}

		private void ThreadedRun()
		{
            mWindow = new UI.RendererWindow();
			mParent.SetWindow(mWindow);
			mWindow.OnGameStart(mSimOutput);

			if (mApplication == null)
			{
				mApplication = new Application();
				mApplication.ShutdownMode = ShutdownMode.OnExplicitShutdown;
			}
			mApplication.Run(mWindow);
		}
	}
}

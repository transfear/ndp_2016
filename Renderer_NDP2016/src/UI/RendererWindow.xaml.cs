﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using FrameworkInterface.Player;
using Simulation_NDP2016.Data;
using Renderer_NDP2016.Data;
using Renderer_NDP2016.Utilities;

namespace Renderer_NDP2016.UI
{
	/// <summary>
	/// Interaction logic for RendererWindow.xaml
	/// </summary>
	public partial class RendererWindow : Window
	{
		public bool CloseRequested { get; set; }

		public RendererWindow()
		{
			InitializeComponent();

            RenderOptions.SetBitmapScalingMode(imgGrid, BitmapScalingMode.NearestNeighbor);

			CloseRequested = false;
			Closing += OnWindowClosing;
		}

        UInt32 mGridWidth  = 1;
        UInt32 mGridHeight = 1;

        Dictionary<IPlayer, PlayerData> mPlayerMap = new Dictionary<IPlayer, PlayerData>();
        Random mRNG = new Random();

		public void OnGameStart(SimulationOutput _simOutput)
		{
            mPlayerMap.Clear();
            grdPlayers.Children.Clear();

            // Generate a random number generator with a fixed seed based on player input
			UInt32 randomSeed = PlayerData.GenerateRandomSeed(_simOutput.players);
            mRNG = new Random((int)randomSeed);

            // Populate our player map
            foreach (SimulationOutput.PlayerData curPlayer in _simOutput.players)
                mPlayerMap[curPlayer.player] = new PlayerData();

            // Assign random color to players
            ColorUtils.AssignRandomColor(mRNG, mPlayerMap);

            // Setup grid
            mGridWidth  = _simOutput.gridWidth;
            mGridHeight = _simOutput.gridHeight;
            WriteableBitmap mainObjective = BitmapUtils.CreateWBFromPlayerData((int)mGridWidth, (int)mGridHeight, _simOutput.players, mPlayerMap.Values.Select(x => x.hue).ToArray());
            imgGrid.Source = mainObjective;

            // Calc max unit count
            int unitCount = _simOutput.players.Select(x => x.units.Count()).Sum();

            // Setup uniform grid
            foreach (SimulationOutput.PlayerData curPlayer in _simOutput.players)
            {
                PlayerData rendererData = mPlayerMap[curPlayer.player];
                PlayerControl uiControl = new PlayerControl(curPlayer, rendererData, _simOutput.turnTime, unitCount);
                rendererData.uiControl = uiControl;

                grdPlayers.Children.Add(uiControl);
            }
		}

        public void UpdateUI(SimulationOutput _simOutput)
		{
            // Update grid
            WriteableBitmap mainObjective = BitmapUtils.CreateWBFromPlayerData((int)mGridWidth, (int)mGridHeight, _simOutput.players, mPlayerMap.Values.Select(x => x.hue).ToArray());
            imgGrid.Source = mainObjective;

            // Update uniform grid
            foreach (SimulationOutput.PlayerData curPlayer in _simOutput.players)
            {
                PlayerData rendererData = mPlayerMap[curPlayer.player];
                rendererData.uiControl.UpdateUI(curPlayer);
            }
		}

		private void OnWindowClosing(object sender, CancelEventArgs e)
		{
			// Always prevent the window from closing - let the simulation decide what to do
			CloseRequested = true;
			e.Cancel = true;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Simulation_NDP2016.Data;

using Renderer_NDP2016.Data;

namespace Renderer_NDP2016.UI
{
    /// <summary>
    /// Interaction logic for PlayerControl.xaml
    /// </summary>
    public partial class PlayerControl : UserControl
    {
        private const int NumUpdatesForAvg = 33;

        private TimeSpan mFrequency;
        private double[] mLastFramesAvg  = new double[PlayerControl.NumUpdatesForAvg];
        private int      mCurFrameIdx    = 0;
        private int      mTotalUnitCount = 0;
        private Utilities.Color myHue;

        public PlayerControl(SimulationOutput.PlayerData simData, PlayerData rendererData, TimeSpan frequency, int iTotalUnitCount)
        {
            InitializeComponent();

            mFrequency      = frequency;
            mTotalUnitCount = iTotalUnitCount;
            myHue           = rendererData.hue;
            Array.Clear(mLastFramesAvg, 0, PlayerControl.NumUpdatesForAvg);

            // Setup labels
            lblPlayerName.Content = simData.player.GetPlayerName();
            lblPlayerName.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, myHue.R, myHue.G, myHue.B));

            UpdateUI(simData);
        }

        public void UpdateUI(SimulationOutput.PlayerData simData)
        {
            double dAllowedMs = mFrequency.TotalMilliseconds;
            double dTookMs = simData.LastTurnTime.TotalMilliseconds;
            mLastFramesAvg[mCurFrameIdx] = dTookMs;

            double dAvgMs = mLastFramesAvg.Average();
            double dTookRatio = dAvgMs / dAllowedMs;

            int numUnits = simData.units.Count();
            lblUnits.Content = numUnits.ToString();
            lblTimeSpent.Content = ((int)dAvgMs).ToString() + " ms";

            // Time Spent background
            {
                LinearGradientBrush bgBrush = new LinearGradientBrush();

                // Initial gradient
                bgBrush.GradientStops.Add(new GradientStop(Color.FromRgb(0, 255, 0), 0.0));

                // Player time end gradient
                double dEndGradient = ((-1.0 / (dTookRatio + 1)) + 1);
                byte r = (byte)(dEndGradient * 255);
                byte g = (byte)((1.0 - dEndGradient) * 255);

                bgBrush.GradientStops.Add(new GradientStop(Color.FromRgb(r, g, 0), dEndGradient));

                // Stack panel end gradient
                bgBrush.GradientStops.Add(new GradientStop(Color.FromRgb(255, 255, 255), dEndGradient));

                stckPnlTime.Background = bgBrush;
            }

            // Unit count background
            {
                LinearGradientBrush bgBrush = new LinearGradientBrush();

                // Initial gradient
                bgBrush.GradientStops.Add(new GradientStop(Color.FromArgb(196, myHue.R, myHue.G, myHue.B), 0.0));

                // Player units end gradient
                double dUnitRatio = (double)numUnits / (double)mTotalUnitCount;
                bgBrush.GradientStops.Add(new GradientStop(Color.FromArgb(80, myHue.R, myHue.G, myHue.B), dUnitRatio));

                // Stack panel end gradient
                bgBrush.GradientStops.Add(new GradientStop(Color.FromRgb(255, 255, 255), dUnitRatio));

                stckPnlUnits.Background = bgBrush;
            }

            mCurFrameIdx = (mCurFrameIdx + 1) % PlayerControl.NumUpdatesForAvg;
        }
    }
}

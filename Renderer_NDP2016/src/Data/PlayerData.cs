﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Simulation_NDP2016.Data;
using Renderer_NDP2016.UI;
using Renderer_NDP2016.Utilities;

namespace Renderer_NDP2016.Data
{
	public class PlayerData
	{
		public Color hue;
        public PlayerControl uiControl;

		public static UInt32 GenerateRandomSeed(SimulationOutput.PlayerData[] _playerList)
		{
			IEnumerable<string> playerNameList = _playerList.Select(x => x.player.GetPlayerName());
			IEnumerable<string> dllPathList    = _playerList.Select(x => x.player.DLLPath);

			StringBuilder sb = new StringBuilder();
			foreach (string curPlayerName in playerNameList)
				sb.Append(curPlayerName);
			foreach (string curDLLPath in dllPathList)
				sb.Append(curDLLPath);

			string concatenatedString = sb.ToString();
			byte[] byteArray = Encoding.ASCII.GetBytes(concatenatedString);

			UInt32 randomSeed = MathUtils.CalculateCRC(byteArray);
			return randomSeed;
		}
	}
}

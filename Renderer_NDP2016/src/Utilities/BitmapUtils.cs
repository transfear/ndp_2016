﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Simulation_NDP2016.Data;

namespace Renderer_NDP2016.Utilities
{
	static class BitmapUtils
	{
        public static WriteableBitmap CreateWBFromPlayerData(int width, int height, SimulationOutput.PlayerData[] players, Color[] playerColors)
		{
            Debug.Assert(players.Count() == playerColors.Count());

			int bufSize = width * height * 3;
			byte[] pixelBuf = new byte[bufSize];
            Array.Clear(pixelBuf, 0, bufSize);

            // Loop on all players
            Int32 numPlayers = players.Count();
            for (int playerIdx = 0; playerIdx < numPlayers; ++playerIdx)
            {
                SimulationOutput.PlayerData playerData = players[playerIdx];
                Color playerColor = playerColors[playerIdx];

                // Loop on all units owned by this player
                foreach (Unit curUnit in playerData.units)
                {
                    int x = curUnit.Pos.x;
                    int y = curUnit.Pos.y;

                    int offset = (y * width + x) * 3;
                    pixelBuf[offset + 0] = playerColor.R;
                    pixelBuf[offset + 1] = playerColor.G;
                    pixelBuf[offset + 2] = playerColor.B;
                }
            }

            // Create the bitmap
			WriteableBitmap wb = new WriteableBitmap(width, height, 96, 96, PixelFormats.Rgb24, null);
			wb.WritePixels(new Int32Rect(0, 0, width, height), pixelBuf, width * 3, 0);

			return wb;
		}
	}
}

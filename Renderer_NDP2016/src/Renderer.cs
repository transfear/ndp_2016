﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

using Simulation_NDP2016.Data;
using FrameworkInterface.Renderer;
using FrameworkInterface.Simulation;

namespace Renderer_NDP2016
{
	public class Renderer : IRenderer
	{
		AutoResetEvent    mWindowSetEvent = new AutoResetEvent(false);
        UI.RendererWindow mWindow         = null;
		UIThread          mUIThread       = null;

		public IRendererFeedback OnGameStart(ISimulationOutput _simulationData)
		{
            SimulationOutput simOutput = _simulationData as SimulationOutput;
			if (simOutput == null)
				return null;

			bool bFirstStart = mUIThread == null;
			if (bFirstStart)
			{
				mUIThread = new UIThread(this);
				mUIThread.StartUIThread(simOutput);
				mWindowSetEvent.WaitOne();
			}

			mWindow.CloseRequested = false;
			return null;
		}

        public void SetWindow(UI.RendererWindow _window)
		{
			mWindow = _window;
			mWindowSetEvent.Set();
		}

		public IRendererFeedback Update(ISimulationOutput _simulationData)
		{
            SimulationOutput simOutput = _simulationData as SimulationOutput;
			if (simOutput == null)
				return null;

			// Create feedback for the simulation
            RendererFeedback feedback = new RendererFeedback();
			feedback.ShouldQuit = false;

			if (mWindow != null)
			{
				feedback.ShouldQuit    = mWindow.CloseRequested;
				feedback.ShouldRestart = false;

				mWindow.Dispatcher.BeginInvoke((Action)(() => { mWindow.UpdateUI(simOutput); }));				
			}

			return feedback;
		}

		public IRendererFeedback OnGameEnd(ISimulationOutput _simulationData)
		{
            RendererFeedback newFeedBack = new RendererFeedback();
			newFeedBack.ShouldQuit    = true;
			newFeedBack.ShouldRestart = false;

            SimulationOutput simOutput = _simulationData as SimulationOutput;
			if (simOutput != null)
			{
				// Attempt to find best player
				Int32 iBestScore = -1;
                SimulationOutput.PlayerData bestPlayer = null;
                foreach (SimulationOutput.PlayerData curPlayer in simOutput.players)
				{
                    Int32 numUnits = curPlayer.units.GetLength(0);
                    if (numUnits > iBestScore)
 					{
                        iBestScore = numUnits;
 						bestPlayer = curPlayer;
 					}
				}

				if (bestPlayer != null)
				{
					UInt32 uiScore = (UInt32)iBestScore;
					MessageBoxResult msgResult =  MessageBox.Show(bestPlayer.player.GetPlayerName() + " won with " + uiScore.ToString() + " units. Would you like to restart?", "Game finished!", MessageBoxButton.YesNo);
					newFeedBack.ShouldRestart = msgResult == MessageBoxResult.Yes;
				}
			}

			// Close the window
			mWindow.Dispatcher.Invoke((Action)(() => 
			{ 
				// Explicit shutdown
				if (!newFeedBack.ShouldRestart)
				{
					mWindow.Close();
					mUIThread.ExplicitShutdown();
				}
			}));

			// Wait for the thread to finish
			if (!newFeedBack.ShouldRestart)
				mUIThread.Join();

			return newFeedBack;
		}
	}
}

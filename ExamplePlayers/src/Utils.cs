﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Simulation_NDP2016.Data;
using FrameworkInterface.Player;

namespace PlayerView
{
    public static class Utils
    {
        private static bool   mbHandleWrapAround = true;
        public  static int    GridWidth          = 1;
        public  static int    GridHeight         = 1;
        private static readonly ThreadLocal<Random> RNG = new ThreadLocal<Random>(() => new Random(Environment.TickCount * Thread.CurrentThread.ManagedThreadId));


        public static int HandleWrapAroundX(int x)
        {
            if (mbHandleWrapAround)
            {
                if (x < 0)
                    x = GridWidth - 1;
                else if (x >= GridWidth)
                    x = 0;
            }
            else
            {
                if (x < 0)
                    x = 0;
                else if (x >= GridWidth)
                    x = GridWidth - 1;
            }

            return x;
        }

        public static int HandleWrapAroundY(int y)
        {
            if (mbHandleWrapAround)
            {
                if (y < 0)
                    y = GridHeight - 1;
                else if (y >= GridHeight)
                    y = 0;
            }
            else
            {
                if (y < 0)
                    y = 0;
                else if (y >= GridHeight)
                    y = GridHeight - 1;
            }

            return y;
        }

        public static Pos2D HandleWrapAround(Pos2D input)
        {
            int gridWidth  = GridWidth;
            int gridHeight = GridHeight;

            Pos2D toReturn = input;
            if (mbHandleWrapAround)
            {
                // Wrap around
                if (toReturn.x < 0)
                    toReturn.x = (int)(gridWidth - 1);
                else if (toReturn.x >= gridWidth)
                    toReturn.x = 0;

                if (toReturn.y < 0)
                    toReturn.y = (int)(gridHeight - 1);
                else if (toReturn.y >= gridHeight)
                    toReturn.y = 0;
            }
            else
            {
                // Clamp on edges
                if (toReturn.x < 0)
                    toReturn.x = 0;
                else if (toReturn.x >= gridWidth)
                    toReturn.x = gridWidth - 1;

                if (toReturn.y < 0)
                    toReturn.y = 0;
                else if (toReturn.y >= gridHeight)
                    toReturn.y = gridHeight - 1;
            }

            return toReturn;
        }

        public static int DistanceTo(Pos2D fromPos, Pos2D toPos)
        {
            int halfGridWidth  = (GridWidth  + 1) / 2;
            int halfGridHeight = (GridHeight + 1) / 2;

            int diffX = Math.Abs(fromPos.x - toPos.x);
            int diffY = Math.Abs(fromPos.y - toPos.y);

            if (diffX > halfGridWidth)
                diffX = GridWidth - diffX;

            if (diffY > halfGridHeight)
                diffY = GridHeight - diffY;

            int dist = diffX + diffY;
            return dist;
        }

        public static Pos2D VectorTo(Pos2D currentPos, Pos2D wantedPos)
        {
            Pos2D diffPos = new Pos2D(wantedPos.x - currentPos.x, wantedPos.y - currentPos.y);

            int halfGridWidth  = (GridWidth  + 1) / 2;
            int halfGridHeight = (GridHeight + 1) / 2;

            Pos2D absPos = new Pos2D(Math.Abs(diffPos.x), Math.Abs(diffPos.y));
            if (absPos.x > halfGridWidth)
            {
                diffPos.x = GridWidth - diffPos.x;
                if (wantedPos.x < currentPos.x)
                    diffPos.x *= -1;
            }

            if (absPos.y > halfGridHeight)
            {
                diffPos.y = GridHeight - diffPos.y;
                if (wantedPos.y > currentPos.y)
                    diffPos.y *= -1;
            }

            return diffPos;
        }

        public static PlayerOutput.Movement_e MoveTo(Pos2D currentPos, Pos2D wantedPos)
        {
            PlayerOutput.Movement_e move = PlayerOutput.Movement_e.kMovement_None;

            Pos2D diffPos = VectorTo(currentPos, wantedPos);
            if (diffPos.x == 0 && diffPos.y == 0)
                return PlayerOutput.Movement_e.kMovement_None;

            Pos2D absPos = new Pos2D(Math.Abs(diffPos.x), Math.Abs(diffPos.y));
            if (absPos.y > absPos.x)
                move = (diffPos.y > 0) ? PlayerOutput.Movement_e.kMovement_Down : PlayerOutput.Movement_e.kMovement_Up;
            else
                move = (diffPos.x > 0) ? PlayerOutput.Movement_e.kMovement_Right : PlayerOutput.Movement_e.kMovement_Left;

            return move;
        }

        public static PlayerOutput.Movement_e MoveTo2(Pos2D currentPos, Pos2D wantedPos)
        {
            PlayerOutput.Movement_e move = PlayerOutput.Movement_e.kMovement_None;

            Pos2D diffPos = VectorTo(currentPos, wantedPos);
            if (diffPos.x == 0 && diffPos.y == 0)
                return PlayerOutput.Movement_e.kMovement_None;

            Pos2D absPos = new Pos2D(Math.Abs(diffPos.x), Math.Abs(diffPos.y));
            int   sum    = absPos.x + absPos.y;
            int   rndNum = RNG.Value.Next() % sum;

            if (rndNum > absPos.x)
                move = (diffPos.y > 0) ? PlayerOutput.Movement_e.kMovement_Down : PlayerOutput.Movement_e.kMovement_Up;
            else
                move = (diffPos.x > 0) ? PlayerOutput.Movement_e.kMovement_Right : PlayerOutput.Movement_e.kMovement_Left;

            return move;
        }
    }
}

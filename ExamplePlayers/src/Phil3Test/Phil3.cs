﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Simulation_NDP2016.Data;
using FrameworkInterface.Player;

namespace Phil3Test
{
    public class Phil3Stats
    {
        // Total
        //	Prolog
        //	Shuffle
        //	AvgMovement                / TotalMovement
        //		AvgBasicMovement       /	TotalBasicMovement
        //		AvgHeadToMaxima        /	TotalHeadToMaxima
        //			AvgFindClosestSpot /		TotalFindClosestSpot
        //			AvgHeadToLocation  /		TotalHeadToLocation
        //		AvgUpdateAgentOnGrid   /	TotalUpdateAgentOnGrid

        // milliseconds
        public long Prolog;
        public long Shuffle;
        public long AvgMovement;
        public long Total;

        // microseconds
        public long TotalBasicMovement;
        public long TotalHeadToMaxima;
        public long TotalFindClosestSpot;
        public long TotalHeadToLocation;
        public long TotalUpdateAgentOnGrid;
        public long TotalMovement;
    }

    public class Phil3 : IPlayer
    {
        private class OnGridUnit
        {
            public Unit unit;
            public uint playerIndex;
        }

		public class DesiredMovement
		{
			public Unit  actualUnit;
			public Pos2D wantedPos;
		}

        const int kiEnemyInfluence  = 5;
        const int kiFriendInfluence = 25;

        Random            mRNG             = new Random();
		uint              muiCheapRandom   = uint.MaxValue;
		OnGridUnit[,]     mUnitGrid        = null;
		DesiredMovement[] mDesiredMovement = null;

        int miGridWidth;
        int miGridHeight;
		int miHalfGridWidth;
		int miHalfGridHeight;

		Heatmap mFriendlyMap = new Heatmap(kiFriendInfluence, true);
        Heatmap mEnemyMap    = new Heatmap(kiEnemyInfluence, false);

		Debug.DebugControl mDebugCtrl = null;

		public Heatmap FriendHeatMap { get { return mFriendlyMap; } }
		public Heatmap EnemyHeatMap { get { return mEnemyMap; } }
		public int GridWidth { get { return miGridWidth; } }
		public int GridHeight { get { return miGridHeight; } }
		public int FriendInfluence { get { return kiFriendInfluence; } }
		public int EnemyInfluence { get { return kiEnemyInfluence; } }
		public DesiredMovement[] DesiredMovements { get { return mDesiredMovement; } }
		public Phil3Stats PerfStats { get; } = new Phil3Stats();

        public Phil3()
        {
			mDebugCtrl = new Debug.DebugControl(this);
        }

        public override string GetPlayerName()
        {
            return "Phil3";
        }

        public override IPlayerOutput OnGameStart(IPlayerInput _input)
        {
            mRNG = new Random();

            PlayerInput input = _input as PlayerInput;
            miGridWidth  = (int)input.GridWidth;
            miGridHeight = (int)input.GridHeight;
			miHalfGridWidth  = (miGridWidth  + 1) / 2;
			miHalfGridHeight = (miGridHeight + 1) / 2;

			PlayerView.Utils.GridWidth  = miGridWidth;
            PlayerView.Utils.GridHeight = miGridHeight;

            mUnitGrid = new OnGridUnit[miGridWidth, miGridHeight];

            mFriendlyMap.Start(input);
            mEnemyMap.Start(input);
            return null;
        }

		public override IPlayerOutput Update(IPlayerInput _input)
        {
			Stopwatch sw = Stopwatch.StartNew();

			PlayerInput input = _input as PlayerInput;
            PlayerOutput output = new PlayerOutput();

            int numAgents = input.MyData.units.Length;
            if (numAgents > 0)
            {
				output.movement  = new PlayerOutput.UnitMovement[numAgents];
				mDesiredMovement = new DesiredMovement[numAgents];

                // Process common read-only stuff for the swarm (eg. heat map)
                PrepareUnitGrid(input);
                mFriendlyMap.PumpInput(input);
                mEnemyMap.PumpInput(input);

				PerfStats.Prolog = sw.ElapsedMilliseconds;

                // Then update the swarm in random order (should be parallelizable)
                List<int> randomList = new List<int>();
                for (int i = 0; i < numAgents; ++i)
                    randomList.Add(i);
                Shuffle(randomList);

				PerfStats.Shuffle = sw.ElapsedMilliseconds - PerfStats.Prolog;

				// Child stats
				PerfStats.TotalMovement          = 0;
				PerfStats.TotalBasicMovement     = 0;
				PerfStats.TotalHeadToMaxima      = 0;
				PerfStats.TotalFindClosestSpot   = 0;
				PerfStats.TotalHeadToLocation    = 0;
				PerfStats.TotalUpdateAgentOnGrid = 0;

				Parallel.ForEach(randomList, (int i) =>
				{
					UpdateAgent(input, output, i);
				});

				PerfStats.AvgMovement = sw.ElapsedMilliseconds - (PerfStats.Shuffle + PerfStats.Prolog);
				
				mDebugCtrl.UpdateUI();
			}

			PerfStats.Total = sw.ElapsedMilliseconds;

			return output;
        }

        public override IPlayerOutput OnGameEnd(IPlayerInput _input)
        {
            mFriendlyMap.Stop();
            mEnemyMap.Stop();

			return null;
        }

        private void Shuffle<T>(IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = mRNG.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        private void PrepareUnitGrid(PlayerInput input)
        {
            // Empty the grid
            for (int x = 0; x < miGridWidth; ++x)
            {
                for (int y = 0; y < miGridHeight; ++y)
                {
                    mUnitGrid[x, y] = null;
                }
            }

            // Place my units on the grids
            foreach (Unit curUnit in input.MyData.units)
            {
                OnGridUnit newUnit  = new OnGridUnit();
                newUnit.playerIndex = input.MyData.playerIndex;
                newUnit.unit        = curUnit;
                mUnitGrid[curUnit.Pos.x, curUnit.Pos.y] = newUnit;
            }

            // Place enemy units on the grid
            foreach (PlayerInput.PlayerData curEnemy in input.EnemyData)
            {
                foreach (Unit curUnit in curEnemy.units)
                {
                    OnGridUnit newUnit  = new OnGridUnit();
                    newUnit.playerIndex = curEnemy.playerIndex;
                    newUnit.unit        = curUnit;
                    mUnitGrid[curUnit.Pos.x, curUnit.Pos.y] = newUnit;
                }
            }
        }

        private void UpdateAgent(PlayerInput input, PlayerOutput output, int agentIdx)
		{
			Stopwatch sw = Stopwatch.StartNew();

            Unit agentUnit = input.MyData.units[agentIdx];

            output.movement[agentIdx].unitID = agentUnit.UniqueID;
            output.movement[agentIdx].move = MoveAgent(agentIdx, agentUnit, input);

			Interlocked.Add(ref PerfStats.TotalMovement, sw.ElapsedTicks * 1000 * 1000 / Stopwatch.Frequency);
        }

        private void UpdateAgentOnGrid(Unit agentUnit, UInt32 playerIndex, PlayerOutput.Movement_e move)
        {
            Pos2D oldPos = agentUnit.Pos;
            Pos2D newPos = oldPos;
            if (move == PlayerOutput.Movement_e.kMovement_Left)
                --newPos.x;
            else if (move == PlayerOutput.Movement_e.kMovement_Right)
                ++newPos.x;
            else if (move == PlayerOutput.Movement_e.kMovement_Up)
                --newPos.y;
            else if (move == PlayerOutput.Movement_e.kMovement_Down)
                ++newPos.y;

            newPos = RealWrapAround(newPos);

            mUnitGrid[newPos.x, newPos.y] = mUnitGrid[oldPos.x, oldPos.y];
            mUnitGrid[oldPos.x, oldPos.y] = null;
        }

		private PlayerOutput.Movement_e MoveAgent(int agentIdx, Unit agentUnit, PlayerInput input)
		{
			Stopwatch sw = Stopwatch.StartNew();

			DesiredMovement desiredMovement = new DesiredMovement() { actualUnit = agentUnit };
			mDesiredMovement[agentIdx] = desiredMovement;

            PlayerOutput.Movement_e toReturn = PlayerOutput.Movement_e.kMovement_Count;

            // Rule #1: never allow another friendly agent to be directly to the right, to the left, to the top or to the bottom of me
            if ((toReturn == PlayerOutput.Movement_e.kMovement_Count) || (toReturn == PlayerOutput.Movement_e.kMovement_None))
            {
                toReturn = ComputeBasicMovement(agentIdx, agentUnit, input, desiredMovement);
                long swBasicMovement = sw.ElapsedTicks * 1000 * 1000 / Stopwatch.Frequency;
                sw.Restart();
                Interlocked.Add(ref PerfStats.TotalBasicMovement, swBasicMovement);
            }

            if ((toReturn == PlayerOutput.Movement_e.kMovement_Count) || (toReturn == PlayerOutput.Movement_e.kMovement_None))
            {
                // Rule #2: try to clump together as much as possible OR try to avoid enemies???
                toReturn = HeadToMaxima(agentIdx, agentUnit, input, desiredMovement);
                long swHeadToMaxima = sw.ElapsedTicks * 1000 * 1000 / Stopwatch.Frequency;
                sw.Restart();
                Interlocked.Add(ref PerfStats.TotalHeadToMaxima, swHeadToMaxima);
            }

			// After all the rules, still doesn't know what to do? Don't move.
			if (toReturn == PlayerOutput.Movement_e.kMovement_Count)
			{
				toReturn = PlayerOutput.Movement_e.kMovement_None;
				desiredMovement.wantedPos = agentUnit.Pos;
			}

            UpdateAgentOnGrid(agentUnit, input.MyData.playerIndex, toReturn);
			
			long swUpdateAgentOnGrid = sw.ElapsedTicks * 1000 * 1000 / Stopwatch.Frequency;
			Interlocked.Add(ref PerfStats.TotalUpdateAgentOnGrid, swUpdateAgentOnGrid);

            return toReturn;
        }

        private class PosScore
        {
            public Pos2D pos;
            public PlayerOutput.Movement_e move;
            public int   score = int.MinValue;
        }

        private PlayerOutput.Movement_e ComputeBasicMovement(int agentIdx, Unit agentUnit, PlayerInput input, DesiredMovement desiredMovement)
        {
            uint myPlayerIdx = input.MyData.playerIndex;
            int  myX = agentUnit.Pos.x;
            int  myY = agentUnit.Pos.y;

			PosScore[] positionScores = new PosScore[]
			{
				new PosScore() { pos = RealWrapAround(new Pos2D(myX, myY - 1)), move = PlayerOutput.Movement_e.kMovement_Up    },
				new PosScore() { pos = RealWrapAround(new Pos2D(myX, myY + 1)), move = PlayerOutput.Movement_e.kMovement_Down  },
				new PosScore() { pos = RealWrapAround(new Pos2D(myX - 1, myY)), move = PlayerOutput.Movement_e.kMovement_Left  },
				new PosScore() { pos = RealWrapAround(new Pos2D(myX + 1, myY)), move = PlayerOutput.Movement_e.kMovement_Right },
				new PosScore() { pos = agentUnit.Pos, move = PlayerOutput.Movement_e.kMovement_None }
			};

			// Calculate potential position scores
			int numPotentialMoves = 0;
			PosScore[] potentialMoves = new PosScore[(int)PlayerOutput.Movement_e.kMovement_Count];
            foreach (PosScore curScore in positionScores)
            {
                Pos2D    curPos    = curScore.pos;
                int      iCurScore = 0;

                if (curScore.move != PlayerOutput.Movement_e.kMovement_None)
                {
                    // Cell is currently occupied, don't move there
                    if (mUnitGrid[curPos.x, curPos.y] != null)
                        continue;
                }

                // Does this cell have neighbors?
                Pos2D[] negNeighbors = new Pos2D[4]
                {
					RealWrapAround(new Pos2D(curPos.x + 1, curPos.y)),
					RealWrapAround(new Pos2D(curPos.x - 1, curPos.y)),
					RealWrapAround(new Pos2D(curPos.x, curPos.y - 1)),
					RealWrapAround(new Pos2D(curPos.x, curPos.y + 1)),
                };
                Pos2D[] posNeighbors = new Pos2D[4]
                {
					RealWrapAround(new Pos2D(curPos.x + 1, curPos.y + 1)),
					RealWrapAround(new Pos2D(curPos.x - 1, curPos.y + 1)),
					RealWrapAround(new Pos2D(curPos.x - 1, curPos.y - 1)),
					RealWrapAround(new Pos2D(curPos.x + 1, curPos.y + 1)),
                };

                foreach (Pos2D curNegNeighbor in negNeighbors)
                {
                    if (mUnitGrid[curNegNeighbor.x, curNegNeighbor.y] != null)
                        iCurScore -= 100;
                }

                foreach (Pos2D curPosNeighbor in posNeighbors)
                {
                    if (mUnitGrid[curPosNeighbor.x, curPosNeighbor.y] != null)
                        iCurScore += 100;
                }

                curScore.score = iCurScore;
				potentialMoves[numPotentialMoves++] = curScore;
            }

            // No valid moves? Don't move
            if (numPotentialMoves == 0)
                return PlayerOutput.Movement_e.kMovement_Count;

			// Sort moves from the best to the worst
			int[] bestScoreIdxArray = new int[(int)PlayerOutput.Movement_e.kMovement_Count];
			bestScoreIdxArray[0] = 0;
			int bestScore = potentialMoves[0].score;
			uint numBestScore = 1;

			for (int i = 1; i < numPotentialMoves; ++i)
			{
				int curScore = potentialMoves[i].score;
				if (curScore > bestScore)
				{
					bestScoreIdxArray[0] = i;
					numBestScore = 1;
					bestScore = curScore;
				}
				else if (curScore == bestScore)
				{
					bestScoreIdxArray[numBestScore++] = i;
				}
			}

            // All moves have equal score? Don't move
            if (numBestScore == (int)(PlayerOutput.Movement_e.kMovement_Count))
                return PlayerOutput.Movement_e.kMovement_Count;

			// Select a random move from the best moves
            uint bestScoreIdx = muiCheapRandom++ % numBestScore;
			int  moveIndex = bestScoreIdxArray[bestScoreIdx];
            PosScore bestMove = potentialMoves[moveIndex];
			desiredMovement.wantedPos = bestMove.pos;

			return bestMove.move;
        }

        private int WrapAroundDist(Pos2D posA, Pos2D posB)
        {
            int distX = Math.Abs(posA.x - posB.x);
            int distY = Math.Abs(posA.y - posB.y);

            if (distX > miHalfGridWidth)
                distX = miGridWidth - distX;

            if (distY > miHalfGridHeight)
                distY = miGridHeight - distY;

            return distX + distY;
        }

        private Heatmap.HeatSpot FindClosestSpot(Pos2D pos, Heatmap.HeatSpot[] spotList, PlayerInput input, int ignoreCloseSpotDist)
        {
            Heatmap.HeatSpot closestSpot = null;
            int smallestDist = int.MaxValue;

			foreach (Heatmap.HeatSpot curSpot in spotList)
			{
				int curDist = WrapAroundDist(pos, curSpot.pos);

                if (curDist < ignoreCloseSpotDist)
                    continue;

                if (curDist < smallestDist)
                {
                    smallestDist = curDist;
                    closestSpot  = curSpot;
                }
            }

			if (closestSpot == null)
				closestSpot = spotList.First();

			return closestSpot;
        }

        private PlayerOutput.Movement_e HeadToLocation(Unit agentUnit, Pos2D wantedPos)
        {
            return PlayerView.Utils.MoveTo2(agentUnit.Pos, wantedPos);
		}

        private PlayerOutput.Movement_e HeadToMaxima(int agentIdx, Unit agentUnit, PlayerInput input, DesiredMovement desiredMovement)
        {
            Heatmap.HeatSpot[] enemyHeatList    = mEnemyMap.GetExtremaList();
            Heatmap.HeatSpot[] friendlyHeatList = mFriendlyMap.GetExtremaList();

            PlayerOutput.Movement_e move = PlayerOutput.Movement_e.kMovement_None;

            // Both lists must contain something
            if (enemyHeatList.Any() && friendlyHeatList.Any())
            {
                int[,] enemyMap    = mEnemyMap.GetHeatMap();
                int[,] friendlyMap = mFriendlyMap.GetHeatMap();

                int enemyScore    = enemyMap[agentUnit.Pos.x, agentUnit.Pos.y];
                int friendlyScore = friendlyMap[agentUnit.Pos.x, agentUnit.Pos.y];

				Stopwatch sw = Stopwatch.StartNew();
                if (enemyScore < friendlyScore)
                {
					// Head to enemies
					Heatmap.HeatSpot enemySpot = FindClosestSpot(agentUnit.Pos, enemyHeatList, input, 1);
					desiredMovement.wantedPos = enemySpot.pos;
					
                }
                else
                {
					// Head to friendlies
					Heatmap.HeatSpot friendlySpot = FindClosestSpot(agentUnit.Pos, friendlyHeatList, input, kiFriendInfluence - 1);
					desiredMovement.wantedPos = friendlySpot.pos;
				}

				long swFindClosestSpot = sw.ElapsedTicks * 1000 * 1000 / Stopwatch.Frequency;
				sw.Restart();
				Interlocked.Add(ref PerfStats.TotalFindClosestSpot, swFindClosestSpot);

				move = HeadToLocation(agentUnit, desiredMovement.wantedPos);
				Interlocked.Add(ref PerfStats.TotalHeadToLocation, sw.ElapsedTicks * 1000 * 1000 / Stopwatch.Frequency);
			}

            /*
            Pos2D[] offsets = new Pos2D[(int)PlayerOutput.Movement_e.kMovement_Count]
            {
                new Pos2D( 0, -1), // PlayerOutput.Movement_e.kMovement_Up
                new Pos2D( 0,  1), // PlayerOutput.Movement_e.kMovement_Down
                new Pos2D(-1,  0), // PlayerOutput.Movement_e.kMovement_Left
                new Pos2D( 1,  0), // PlayerOutput.Movement_e.kMovement_Right
                new Pos2D( 0,  0)  // PlayerOutput.Movement_e.kMovement_None
            };

            Pos2D nextPos = new Pos2D(RealWrapAroundX(agentUnit.Pos.x + offsets[(int)move].x), RealWrapAroundY(agentUnit.Pos.y + offsets[(int)move].y));
            if (mUnitGrid[nextPos.x, nextPos.y] != null)
                move = PlayerOutput.Movement_e.kMovement_None;
            */

            return move;
        }

		private Pos2D RealWrapAround(Pos2D p)
		{
			return new Pos2D() { x = RealWrapAroundX(p.x), y = RealWrapAroundY(p.y) };
		}

		private int RealWrapAroundX(int x)
		{
			if (x < 0)
			{
				return miGridWidth + x;
			}
			if (x >= miGridWidth)
			{
				return x - miGridWidth;
			}
			return x;
		}

		private int RealWrapAroundY(int y)
		{
			if (y < 0)
			{
				return miGridHeight + y;
			}
			if (y >= miGridHeight)
			{
				return y - miGridHeight;
			}
			return y;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace Phil3Test.Debug
{
	/// <summary>
	/// Interaction logic for HeatmapView.xaml
	/// </summary>
	public partial class HeatmapView : Window
	{
		public HeatmapView()
		{
			InitializeComponent();
			RenderOptions.SetBitmapScalingMode(imgHeatmap, BitmapScalingMode.NearestNeighbor);
		}
		
		public bool TrackOtherUnit { get; set; }

		public void UpdateBitmap(int w, int h, byte[] pixelBuf)
		{
			// Create the bitmap
			WriteableBitmap wb = new WriteableBitmap(w, h, 96, 96, PixelFormats.Rgb24, null);
			wb.WritePixels(new Int32Rect(0, 0, w, h), pixelBuf, w * 3, 0);

			// Set it in the window
			imgHeatmap.Source = wb;
		}

		public void UpdateFriendlyHeatmap(Heatmap.Stats perfStats)
		{
			lblFriendlyPrologue.Content     = String.Format("{0} ms", perfStats.Prolog.ToString());
			lblFriendlyInfluenceMap.Content = String.Format("{0} ms", perfStats.InfluenceMap.ToString());
			lblFriendlyExtrema.Content      = String.Format("{0} ms", perfStats.Extrema.ToString());
			lblFriendlySort.Content         = String.Format("{0} ms", perfStats.Sort.ToString());
			lblFriendlyEpilog.Content       = String.Format("{0} ms", perfStats.Epilog.ToString());
			lblFriendlyTotal.Content        = String.Format("{0} ms", perfStats.Total.ToString());
		}

		public void UpdateEnemyHeatmap(Heatmap.Stats perfStats)
		{
			lblEnemyPrologue.Content     = String.Format("{0} ms", perfStats.Prolog.ToString());
			lblEnemyInfluenceMap.Content = String.Format("{0} ms", perfStats.InfluenceMap.ToString());
			lblEnemyExtrema.Content      = String.Format("{0} ms", perfStats.Extrema.ToString());
			lblEnemySort.Content         = String.Format("{0} ms", perfStats.Sort.ToString());
			lblEnemyEpilog.Content       = String.Format("{0} ms", perfStats.Epilog.ToString());
			lblEnemyTotal.Content        = String.Format("{0} ms", perfStats.Total.ToString());
		}

		public void UpdateAIStats(Phil3Stats perfStats, int numAIs)
		{
			long totalMovementMs          = perfStats.TotalMovement          / 1000;
			long totalBasicMovementMs     = perfStats.TotalBasicMovement     / 1000;
			long totalHeadToMaximaMs      = perfStats.TotalHeadToMaxima      / 1000;
			long totalFindClosestSpotMs   = perfStats.TotalFindClosestSpot   / 1000;
			long totalHeadToLocationMs    = perfStats.TotalHeadToLocation    / 1000;
			long totalUpdateAgentOnGridMs = perfStats.TotalUpdateAgentOnGrid / 1000;

			long avgBasicMovementMs     = totalBasicMovementMs     / numAIs;
			long avgHeadToMaximaMs      = totalHeadToMaximaMs      / numAIs;
			long avgFindClosesSpotMs    = totalFindClosestSpotMs   / numAIs;
			long avgHeadToLocationMs    = totalHeadToLocationMs    / numAIs;
			long avgUpdateAgentOnGridMs = totalUpdateAgentOnGridMs / numAIs;

			lblAIProlog.Content  = String.Format("{0} ms", perfStats.Prolog.ToString());
			lblAIShuffle.Content = String.Format("{0} ms", perfStats.Shuffle.ToString());

			lblAIAvgMovement.Content   = String.Format("{0} ms", perfStats.AvgMovement.ToString());
			lblAITotalMovement.Content = String.Format("{0} ms", totalMovementMs.ToString());

			lblAIAvgBasicMovement.Content   = String.Format("{0} ms", avgBasicMovementMs.ToString());
			lblAITotalBasicMovement.Content = String.Format("{0} ms", totalBasicMovementMs.ToString());

			lblAIAvgHeadToMaxima.Content   = String.Format("{0} ms", avgHeadToMaximaMs.ToString());
			lblAITotalHeadToMaxima.Content = String.Format("{0} ms", totalHeadToMaximaMs.ToString());

			lblAIAvgFindClosestSpot.Content   = String.Format("{0} ms", avgFindClosesSpotMs.ToString());
			lblAITotalFindClosestSpot.Content = String.Format("{0} ms", totalFindClosestSpotMs.ToString());

			lblAIAvgHeadToLocation.Content   = String.Format("{0} ms", avgHeadToLocationMs.ToString());
			lblAITotalHeadToLocation.Content = String.Format("{0} ms", totalHeadToLocationMs.ToString());

			lblAIAvgUpdateAgentOnGrid.Content   = String.Format("{0} ms", avgUpdateAgentOnGridMs.ToString());
			lblAITotalUpdateAgentOnGrid.Content = String.Format("{0} ms", totalUpdateAgentOnGridMs.ToString());

			lblAITotal.Content = String.Format("{0} ms", perfStats.Total.ToString()); ;
		}

		private void btnTrackOtherUnit_Click(object sender, RoutedEventArgs e)
		{
			TrackOtherUnit = true;
		}
	}
}

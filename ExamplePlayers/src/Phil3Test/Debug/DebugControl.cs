﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

using Simulation_NDP2016.Data;

namespace Phil3Test.Debug
{
	internal class DebugControl
	{
		private Phil3 mAI = null;
		private Thread STAThread { get; set; }

		public Debug.HeatmapView DebugView { get; set; }

		const bool kDrawHeatmap    = true;
		const bool kDrawSingleUnit = true;
		const bool kDrawPerfStats  = true;

		private Heatmap.Stats mFriendlyStats = new Heatmap.Stats();
		private Heatmap.Stats mEnemyStats    = new Heatmap.Stats();
		private Phil3Stats    mAIStats       = new Phil3Stats();

		private Random mRNG = new Random();
		private uint   mTrackingUnit = uint.MaxValue;
		private int    mNumAgents    = 0;

		public DebugControl(Phil3 _ai)
		{
			// Main runtime switch
			if (File.Exists("P3Debug.txt"))
			{
				mAI = _ai;
				SpawnHeatmapView(this);
			}
		}

		public void UpdateUI()
		{
			if (mAI == null || DebugView == null)
				return;

			Phil3.DesiredMovement[] desiredMovement = mAI.DesiredMovements;
			DebugView.Dispatcher.BeginInvoke(new Action(() => { ThreadedUpdateUI(desiredMovement); }));
		}

		private void ThreadedUpdateUI(Phil3.DesiredMovement[] desiredMovement)
		{
			mNumAgents = desiredMovement.Count();

			int w = mAI.GridWidth;
			int h = mAI.GridHeight;

			int    bufSize  = w * h * 3;
			byte[] pixelBuf = new byte[bufSize];
			Array.Clear(pixelBuf, 0, bufSize);

			if (kDrawHeatmap)
				DrawHeatmap(w, h, pixelBuf);

			if (kDrawSingleUnit)
				DrawSingleUnit(w, h, pixelBuf, desiredMovement);

			DebugView.UpdateBitmap(w, h, pixelBuf);

			if (kDrawPerfStats)
				DrawPerfStats();
		}

		private void DrawPerfStats()
		{
			ThrottleHeatmapPerfStats(mFriendlyStats, mAI.FriendHeatMap.PerfStats);
			ThrottleHeatmapPerfStats(mEnemyStats,    mAI.EnemyHeatMap.PerfStats);
			ThrottleAIPerfStats(mAIStats, mAI.PerfStats);

			DebugView.UpdateFriendlyHeatmap(mFriendlyStats);
			DebugView.UpdateEnemyHeatmap(mEnemyStats);
			DebugView.UpdateAIStats(mAIStats, mNumAgents);
		}

		private void ThrottleHeatmapPerfStats(Heatmap.Stats stored, Heatmap.Stats newStats)
		{
			stored.Prolog       = (long)LerpStat(stored.Prolog, newStats.Prolog);
			stored.InfluenceMap = (long)LerpStat(stored.InfluenceMap, newStats.InfluenceMap);
			stored.Extrema      = (long)LerpStat(stored.Extrema, newStats.Extrema);
			stored.Sort         = (long)LerpStat(stored.Sort, newStats.Sort);
			stored.Epilog       = (long)LerpStat(stored.Epilog, newStats.Epilog);
			stored.Total        = (long)LerpStat(stored.Total, newStats.Total);
		}

		private void ThrottleAIPerfStats(Phil3Stats stored, Phil3Stats newStats)
		{
			stored.Prolog                 = (long)LerpStat(stored.Prolog, newStats.Prolog);
			stored.Shuffle                = (long)LerpStat(stored.Shuffle, newStats.Shuffle);
			stored.AvgMovement            = (long)LerpStat(stored.AvgMovement, newStats.AvgMovement);
			stored.Total                  = (long)LerpStat(stored.Total, newStats.Total);
			stored.TotalBasicMovement     = (long)LerpStat(stored.TotalBasicMovement, newStats.TotalBasicMovement);
			stored.TotalHeadToMaxima      = (long)LerpStat(stored.TotalHeadToMaxima, newStats.TotalHeadToMaxima);
			stored.TotalFindClosestSpot   = (long)LerpStat(stored.TotalFindClosestSpot, newStats.TotalFindClosestSpot);
			stored.TotalHeadToLocation    = (long)LerpStat(stored.TotalHeadToLocation, newStats.TotalHeadToLocation);
			stored.TotalUpdateAgentOnGrid = (long)LerpStat(stored.TotalUpdateAgentOnGrid, newStats.TotalUpdateAgentOnGrid);
			stored.TotalMovement          = (long)LerpStat(stored.TotalMovement, newStats.TotalMovement);
		}

		private float LerpStat(float oldVal, float newVal)
		{
			float fLerp = 0.9f;
			float fInvLerp = 1.0f - fLerp;

			return oldVal * fLerp + fInvLerp * newVal;
		}

		private void DrawSingleUnit(int w, int h, byte[] pixelBuf, Phil3.DesiredMovement[] desiredMovement)
		{
			if (mNumAgents == 0)
				return;

			Phil3.DesiredMovement trackedUnit = desiredMovement.FirstOrDefault(x => x.actualUnit.UniqueID == mTrackingUnit);
			if (trackedUnit == null || DebugView.TrackOtherUnit)
			{
				trackedUnit   = desiredMovement.ElementAt(mRNG.Next(mNumAgents));
				mTrackingUnit = trackedUnit.actualUnit.UniqueID;
				DebugView.TrackOtherUnit = false;
			}

			int currentPosOffset = (trackedUnit.actualUnit.Pos.x + trackedUnit.actualUnit.Pos.y * w) * 3;
			int wantedPosOffset  = (trackedUnit.wantedPos.x      + trackedUnit.wantedPos.y      * w) * 3;

			// current pos = cyan
			pixelBuf[currentPosOffset + 0] = 255;
			pixelBuf[currentPosOffset + 1] = 255;
			pixelBuf[currentPosOffset + 2] = 255;

			// wanted pos = white
			pixelBuf[wantedPosOffset + 0] = 255;
			pixelBuf[wantedPosOffset + 1] = 255;
			pixelBuf[wantedPosOffset + 2] = 255;
		}

		private void DrawHeatmap(int w, int h, byte[] pixelBuf)
		{
			float fInvEnemyInfluence = 1.0f / (float)mAI.EnemyInfluence;
			float fInvFriendInfluence = 1.0f / (float)mAI.FriendInfluence;

			int[,] enemyData    = mAI.EnemyHeatMap.GetHeatMap();
			int[,] friendlyData = mAI.FriendHeatMap.GetHeatMap();

            float fEnemyMax = 255.0f;
            if (enemyData != null)
            {

                foreach (int v in enemyData)
                    fEnemyMax = Math.Max(fEnemyMax, (float)v * fInvEnemyInfluence);

                AccumHeatMap(pixelBuf, w, h, fInvEnemyInfluence / fEnemyMax, enemyData, 0);
            }

            float fFriendMax = 255.0f;
            if (friendlyData != null)
            {
                foreach (int v in friendlyData)
                    fFriendMax = Math.Max(fFriendMax, (float)v * fInvFriendInfluence);

                AccumHeatMap(pixelBuf, w, h, fInvFriendInfluence / fFriendMax, friendlyData, 1);
            }
		}

		private void AccumHeatMap(byte[] pixelBuf, int w, int h, float fNormalizator, int[,] hmData, int offset)
		{
			int idx = 0;
			for (int y = 0; y < h; ++y)
			{
				for (int x = 0; x < w; ++x)
				{
					int heatmapVal = hmData[x, y];
					float fNormalized = (float)heatmapVal * fNormalizator;
					fNormalized = (float)Math.Pow(fNormalized, 0.5);

					float fVal = fNormalized * 255;

					pixelBuf[idx + offset] = (byte)fVal;
					idx += 3;
				}
			}
		}

        private static EventWaitHandle ewh = new EventWaitHandle(false, EventResetMode.ManualReset);

        private static void SpawnHeatmapView(DebugControl dbgCtrl)
		{

			Thread staThread = new Thread(new ParameterizedThreadStart(ThreadedRun));
			staThread.SetApartmentState(ApartmentState.STA);
			staThread.Start(dbgCtrl);

            ewh.WaitOne();
        }

		private static void ThreadedRun(object o)
		{
			DebugControl dbgCtrl =(DebugControl)o;

			HeatmapView hmv = new HeatmapView();
			dbgCtrl.DebugView = hmv;

            ewh.Set();
            hmv.ShowDialog();
		}
	}
}

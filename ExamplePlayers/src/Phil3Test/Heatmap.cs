﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Simulation_NDP2016.Data;
using FrameworkInterface.Player;


namespace Phil3Test
{
    public class Heatmap
    {
		public class Stats
		{
			public long Prolog { get; set; }
			public long InfluenceMap { get; set; }
			public long Extrema { get; set; }
			public long Sort { get; set; }
			public long Epilog { get; set; }
			public long Total { get; set; }
		}
        public class HeatSpot
        {
            [Flags]
            public enum eFlags
            {
                kFlag_None = 0x0,
                kFlag_Checked = 0x1,
                kFlag_Pending = 0x2
            }

            public int score;
            public eFlags flags;
            public Pos2D pos;
            public HeatSpot[] neighbors = new HeatSpot[8];
        }

		int[] miInfluenceScore = null; // (miInfluenceLength * 2 + 1)²
		int[] miWrappedAroundX = null; // miGridWidth  + miInfluenceLength * 2
		int[] miWrappedAroundY = null; // miGridHeight + miInfluenceLength * 2
		int  miInfluenceLength;
        bool mbFriendly;
        int  miGridWidth;
        int  miGridHeight;

        HeatSpot[] mInternalHeatMap = null;	// miGridWidth * miGridHeight
        int[,] mExposedHeatMap      = null;

		HeatSpot[] mExtremaList = new HeatSpot[0];

        // Threading stuff
        bool   mbRunning = false;
        Thread mAsyncThread = null;
        ManualResetEvent mJobReady = new ManualResetEvent(false);
        PlayerInput mNextInput = null;

		// Perf stats
		public Stats PerfStats { get; } = new Stats();

		public Heatmap(int influence, bool bFriendly)
        {
            miInfluenceLength = influence;
            mbFriendly        = bFriendly;
        }

		public bool IsFriendly { get { return mbFriendly; } }

		private int RealWrapAroundX(int x)
		{
			if (x < 0)
			{
				return miGridWidth + x;
			}
			if (x >= miGridWidth)
			{
				return x - miGridWidth;
			}
			return x;
		}

		private int RealWrapAroundY(int y)
		{
			if (y < 0)
			{
				return miGridHeight + y;
			}
			if (y >= miGridHeight)
			{
				return y - miGridHeight;
			}
			return y;
		}

		private void BuildStaticMaps()
		{
			// Influence score
			int sideSize = miInfluenceLength * 2 + 1;
			int totalSize = sideSize * sideSize;
			miInfluenceScore = new int[totalSize];

			int index = 0;
			for (int y = -miInfluenceLength; y <= miInfluenceLength; ++y)
			{
				for (int x = -miInfluenceLength; x <= miInfluenceLength; ++x)
				{
					System.Diagnostics.Debug.Assert(index < totalSize);

					int distToUnit = (miInfluenceLength - Math.Abs(x)) + (miInfluenceLength - Math.Abs(y));
					miInfluenceScore[index] = distToUnit;
					++index;
				}
			}

			// Wrapped around maps
			index = 0;
			totalSize = miGridWidth + miInfluenceLength * 2;
			miWrappedAroundX = new int[totalSize];
			int endX = miGridWidth + miInfluenceLength;
			for (int val = -miInfluenceLength; val < endX; ++val)
			{
				System.Diagnostics.Debug.Assert(index < totalSize);
				miWrappedAroundX[index] = RealWrapAroundX(val);
				++index;
			}

			index = 0;
			totalSize = miGridHeight + miInfluenceLength * 2;
			miWrappedAroundY = new int[totalSize];
			int endY = miGridHeight + miInfluenceLength;
			for (int val = -miInfluenceLength; val < endY; ++val)
			{
				System.Diagnostics.Debug.Assert(index < totalSize);
				miWrappedAroundY[index] = RealWrapAroundY(val);
				++index;
			}
		}


		public void Start(PlayerInput input)
        {
            miGridWidth  = (int)input.GridWidth;
            miGridHeight = (int)input.GridHeight;

            mInternalHeatMap = new HeatSpot[miGridWidth * miGridHeight];

			int gridIdx = 0;

			for (int y = 0; y < miGridHeight; ++y)
			{
                
				for (int x = 0; x < miGridWidth; ++x)
				{
                    mInternalHeatMap[gridIdx] = new HeatSpot() { pos = new Pos2D(x, y) };
					++gridIdx;
                }
            }

			gridIdx = 0;
			for (int y = 0; y < miGridHeight; ++y)
            {
				for (int x = 0; x < miGridWidth; ++x)
				{
                    SetupNeighbors(mInternalHeatMap[gridIdx], x, y);
					++gridIdx;
				}
            }

			BuildStaticMaps();

			// Start async thread
			mbRunning = true;
            mJobReady.Reset();
            mAsyncThread = new Thread(AsyncUpdate);
            mAsyncThread.Start();
        }

        public void Stop()
        {
            mbRunning = false;
            mJobReady.Set();
            mAsyncThread.Join();
        }

        public void PumpInput(PlayerInput input)
        {
            mNextInput = input;
            mJobReady.Set();
        }

        public HeatSpot[] GetExtremaList()
        {
            return mExtremaList;
        }

        public int[,] GetHeatMap()
        {
            return mExposedHeatMap;
        }

        private void SetupNeighbors(HeatSpot cell, int x, int y)
        {
            int leftX  = RealWrapAroundX(cell.pos.x - 1);
            int rightX = RealWrapAroundX(cell.pos.x + 1);
            int upY    = RealWrapAroundY(cell.pos.y - 1);
            int downY  = RealWrapAroundY(cell.pos.y + 1);

			int upYIndexOffset   = miGridWidth * upY;
			int yIndexOffset     = miGridWidth * y;
			int downYIndexOffset = miGridWidth * downY;

			cell.neighbors[0] = mInternalHeatMap[leftX  + upYIndexOffset];
			cell.neighbors[1] = mInternalHeatMap[x      + upYIndexOffset];
			cell.neighbors[2] = mInternalHeatMap[rightX + upYIndexOffset];
			cell.neighbors[3] = mInternalHeatMap[leftX  + yIndexOffset];
			cell.neighbors[4] = mInternalHeatMap[rightX + yIndexOffset];
			cell.neighbors[5] = mInternalHeatMap[leftX  + downYIndexOffset];
			cell.neighbors[6] = mInternalHeatMap[x      + downYIndexOffset];
			cell.neighbors[7] = mInternalHeatMap[rightX + downYIndexOffset];
        }

        private void AsyncUpdate()
        {
            // Loop until asked to kill thread
            while (mbRunning)
            {
                // Wait for next input
                mJobReady.WaitOne();
                if (!mbRunning)
                    break;

				Stopwatch sw = Stopwatch.StartNew();

                // Copy inputs
                PlayerInput curInput = (PlayerInput)mNextInput.CopyDeep();

				// Reset the heatmap to base score
				foreach (HeatSpot curSpot in mInternalHeatMap)
				{
					curSpot.score = 0;
					curSpot.flags = HeatSpot.eFlags.kFlag_None;
				}

				PerfStats.Prolog = sw.ElapsedMilliseconds;
				sw.Restart();

                GenerateInfluenceMap(curInput);

				PerfStats.InfluenceMap = sw.ElapsedMilliseconds;
				sw.Restart();

				// Find peaks
				List<HeatSpot> extremaList = FindLocalExtrema();

				PerfStats.Extrema = sw.ElapsedMilliseconds;
				sw.Restart();

				extremaList.Sort((a, b) => b.score - a.score);

				PerfStats.Sort = sw.ElapsedMilliseconds;
				sw.Restart();

				// Produce exposed heatmap
				int[,] exposedHeatMap = new int[miGridWidth, miGridHeight];
				int gridIdx = 0;
				for (int y = 0; y < miGridHeight; ++y)
                {
					for (int x = 0; x < miGridWidth; ++x)
					{
                        exposedHeatMap[x, y] = mInternalHeatMap[gridIdx].score;
						++gridIdx;
                    }
                }

                // Set outputs as available
                mExtremaList    = extremaList.ToArray();
                mExposedHeatMap = exposedHeatMap;

				PerfStats.Epilog = sw.ElapsedMilliseconds;
				PerfStats.Total  = PerfStats.Prolog + PerfStats.InfluenceMap + PerfStats.Extrema + PerfStats.Sort + PerfStats.Epilog;
            }
        }

        private List<HeatSpot> FindLocalExtrema()
        {
            List<HeatSpot> toReturn = new List<HeatSpot>();

			int ringBufSize  = miGridWidth * miGridHeight;
			int ringBufRead  = 0;
			int ringBufWrite = 0;
			HeatSpot[] spotToCheckRingBuf = new HeatSpot[ringBufSize];

			int pendingToAddCount = 0;
			HeatSpot[] pendingToAdd = new HeatSpot[ringBufSize];

			// TODO: multithread / parallelize?

			foreach (HeatSpot curSpot in  mInternalHeatMap)
			{
				// Ignore already checked spot
				if ((curSpot.flags & HeatSpot.eFlags.kFlag_Checked) != 0)
					continue;

				bool isExtrema = true;
				int originalScore = curSpot.score;
				
				spotToCheckRingBuf[ringBufWrite++] = curSpot;
				ringBufWrite %= ringBufSize;

				pendingToAdd[pendingToAddCount++] = curSpot;
				curSpot.flags |= HeatSpot.eFlags.kFlag_Pending;

				while (ringBufRead != ringBufWrite)
				{
					HeatSpot curSpotToCheck = spotToCheckRingBuf[ringBufRead++];
					ringBufRead %= ringBufSize;

					curSpotToCheck.flags |= HeatSpot.eFlags.kFlag_Checked;

					// This spot is an extrema only if all neighbors are equals or inferior
					foreach (HeatSpot neighbor in curSpotToCheck.neighbors)
					{
						if (neighbor.score > originalScore)
						{
							// If superior score, we're not in an extrema, break and ignore.
							isExtrema = false;
						}
						else if (neighbor.score == originalScore)
						{
							// If equal score, add to the list.
							if ((neighbor.flags & (HeatSpot.eFlags.kFlag_Checked | HeatSpot.eFlags.kFlag_Pending)) == 0)
							{
								spotToCheckRingBuf[ringBufWrite++] = neighbor;
								ringBufWrite %= ringBufSize;

								pendingToAdd[pendingToAddCount++] = neighbor;
								neighbor.flags |= HeatSpot.eFlags.kFlag_Pending;
							}
						}
					}
				}

				if (isExtrema)
				{
					for (int pendingToAddIdx = 0; pendingToAddIdx < pendingToAddCount; ++pendingToAddIdx)
						toReturn.Add(pendingToAdd[pendingToAddIdx]);
				}

				pendingToAddCount = 0;
			}

            return toReturn;
        }

        private void GenerateInfluenceMap(PlayerInput input)
        {
			var unitCollection = (mbFriendly) ? input.MyData.units : input.EnemyData.SelectMany(x => x.units);
			Parallel.ForEach(unitCollection, (Unit curUnit) =>
			{
				ApplyInfluence(curUnit);
			});
        }

		private void ApplyInfluence(Unit curUnit)
		{
			// Influence miInfluenceLength pixels around all units

			int baseX = curUnit.Pos.x + miInfluenceLength;
			int baseY = curUnit.Pos.y + miInfluenceLength;

			int influenceIndex = 0;
			for (int y = -miInfluenceLength; y <= miInfluenceLength; ++y)
			{
				int newY = miWrappedAroundY[baseY + y];

				for (int x = -miInfluenceLength; x <= miInfluenceLength; ++x)
				{
					int newX           = miWrappedAroundX[baseX + x];
					int influenceScore = miInfluenceScore[influenceIndex];
					++influenceIndex;

					int gridIdx = newX + newY * miGridWidth;
					Interlocked.Add(ref mInternalHeatMap[gridIdx].score,  influenceScore);
				}
			}
		}

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Simulation_NDP2016.Data;
using FrameworkInterface.Player;

namespace ExamplePlayer
{
    internal class ExamplePlayer : IPlayer
    {
        public ExamplePlayer() { }

        public override string GetPlayerName()
        {
            return "Example player";
        }

        public override IPlayerOutput OnGameStart(IPlayerInput _input)
        {
            return null;
        }

        public override IPlayerOutput Update(IPlayerInput _input)
        {
            PlayerInput input = _input as PlayerInput;
            PlayerOutput output = new PlayerOutput();

            output.movement = new PlayerOutput.UnitMovement[input.MyData.units.Length];
            for (int i = 0; i < output.movement.Length; ++i)
            {
                output.movement[i].unitID = input.MyData.units[i].UniqueID;
                output.movement[i].move = PlayerOutput.Movement_e.kMovement_Down;
            }

            return output;
        }

        public override IPlayerOutput OnGameEnd(IPlayerInput _input)
        {
            return null;
        }
    }
}

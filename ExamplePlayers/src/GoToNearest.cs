﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Simulation_NDP2016.Data;
using FrameworkInterface.Player;

namespace GoToNearest
{
    internal class GoToNearest : IPlayer
    {
        public GoToNearest() { }

        public override string GetPlayerName()
        {
            return "GoToNearest";
        }

        public override IPlayerOutput OnGameStart(IPlayerInput _input)
        {
            PlayerInput input = _input as PlayerInput;

            PlayerView.Utils.GridWidth  = (int)input.GridWidth;
            PlayerView.Utils.GridHeight = (int)input.GridHeight;

            return null;
        }

        public override IPlayerOutput Update(IPlayerInput _input)
        {
            PlayerInput  input  = _input as PlayerInput;
            PlayerOutput output = new PlayerOutput();

            output.movement = new PlayerOutput.UnitMovement[input.MyData.units.Length];
            for (int i = 0; i < output.movement.Length; ++i)
            {
                output.movement[i].unitID = input.MyData.units[i].UniqueID;
                output.movement[i].move = MoveToClosestEnemy(i, input);
            }

            return output;
        }

        public override IPlayerOutput OnGameEnd(IPlayerInput _input)
        {
            return null;
        }

        private PlayerOutput.Movement_e MoveToClosestEnemy(int unitIndex, PlayerInput input)
        {
            Pos2D myPos = input.MyData.units[unitIndex].Pos;
            
            Pos2D wantedPos = myPos;
            int   bestDist  = int.MaxValue;

            // Loop on all enemies
            foreach (PlayerInput.PlayerData curEnemy in input.EnemyData)
            {
                // Loop on all enemy agents
                foreach (Unit enemyUnit in curEnemy.units)
                {
                    Pos2D enemyPos = enemyUnit.Pos;
                    int dist = PlayerView.Utils.DistanceTo(myPos, enemyPos);
                    if (dist < bestDist)
                    {
                        bestDist  = dist;
                        wantedPos = enemyPos;
                    }
                }
            }

            PlayerOutput.Movement_e toReturn = PlayerView.Utils.MoveTo(myPos, wantedPos);
            return toReturn;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Simulation_NDP2016.Data;

using FrameworkInterface.Player;
using FrameworkInterface.Renderer;
using FrameworkInterface.Simulation;

namespace Simulation_NDP2016.Internal
{
	class BaseSimulation
	{
		#region Public Interface
		public ICollection<IPlayer> PlayerList    { get; set; }

		public void OnGameStart(ICollection<IPlayer> _playerCollection)
		{
			PlayerList = _playerCollection;
			Init();
		}

		public IPlayerInput ProducePlayerInput(IPlayer _player)
		{
			Int32 iNumPlayers = PlayerList.Count;

			PlayerInput toReturn = new PlayerInput();
            toReturn.GridWidth = muiGridWidth;
            toReturn.GridHeight = muiGridHeight;

            // Find current player's data
            PlayerData curData = mPlayerMap[_player];
            Int32 numUnits = curData.units.Count();

            toReturn.MyData = new PlayerInput.PlayerData();
            toReturn.MyData.playerIndex = _player.PlayerIndex;
            toReturn.MyData.units = new Unit[numUnits];

            int curUnitIdx = 0;
            foreach (Unit curUnit in curData.units.Values)
            {
                toReturn.MyData.units[curUnitIdx] = curUnit.CopyDeep();
                ++curUnitIdx;
            }

            // Fill enemy players data
            int curEnemyIdx = 0;
            toReturn.EnemyData = new PlayerInput.PlayerData[iNumPlayers - 1];
            foreach (KeyValuePair<IPlayer, PlayerData> curPair in mPlayerMap)
            {
                // Skip current player, we only want its enemies
                IPlayer curPlayer = curPair.Key;
                if (curPlayer == _player)
                    continue;

                PlayerData enemyData = curPair.Value;
                numUnits = enemyData.units.Count();

                PlayerInput.PlayerData enemyPlayerData = new PlayerInput.PlayerData();
                toReturn.EnemyData[curEnemyIdx] = enemyPlayerData;
                enemyPlayerData.playerIndex = curPlayer.PlayerIndex;
                enemyPlayerData.units       = new Unit[numUnits];

                curUnitIdx = 0;
                foreach (Unit curUnit in enemyData.units.Values)
                {
                    enemyPlayerData.units[curUnitIdx] = curUnit.CopyDeep();
                    ++curUnitIdx;
                }

                ++curEnemyIdx;
            }

			return toReturn;
		}

		public void ApplyPlayerOuput(IPlayer _player, IPlayerOutput _playerResponse)
		{
            PlayerData curData = mPlayerMap[_player];
            curData.LastTurnTime = _playerResponse.TurnTime;

			PlayerOutput response = _playerResponse as PlayerOutput;
			if (response == null || response.movement == null)
				return;

            int moveCount = -1;
            int length = response.movement.Length;
            while (moveCount != 0 && length > 0)
            {
               moveCount = 0;
               for (int i = 0; i < length; ++i)
               {
                    if (response.movement[i].move == PlayerOutput.Movement_e.kMovement_Count)
                        continue;

                    //make sure the unit is still under our control
                    if (curData.units.ContainsKey(response.movement[i].unitID))
                    {
                        Unit curUnit = curData.units[response.movement[i].unitID];
                        if (TryApplyMovement(response.movement[i], curUnit.Pos))
                        {
                            ++moveCount;
                            response.movement[i].move = PlayerOutput.Movement_e.kMovement_Count;
                        }
                    }
               }
            }	
		}

        private Pos2D HandleWrapAround(Pos2D input)
        {
            Pos2D toReturn = input;
            if (!mbWrapAround)
                return toReturn;

            if (toReturn.x < 0)
                toReturn.x = (int)(muiGridWidth - 1);
            else if (toReturn.x >= muiGridWidth)
                toReturn.x = 0;

            if (toReturn.y < 0)
                toReturn.y = (int)(muiGridHeight - 1);
            else if (toReturn.y >= muiGridHeight)
                toReturn.y = 0;

            return toReturn;
        }

        private bool TryApplyMovement(PlayerOutput.UnitMovement _unitWantedMove, Pos2D _unitCurPos)
        {
            Pos2D wantedPos = _unitCurPos;
            switch (_unitWantedMove.move)
            {
                case PlayerOutput.Movement_e.kMovement_Down:
                    {
                        wantedPos.y += 1;
                        break;
                    }
                case PlayerOutput.Movement_e.kMovement_Left:
                    {
                        wantedPos.x -= 1;
                        break;
                    }
                case PlayerOutput.Movement_e.kMovement_Right:
                    {
                        wantedPos.x += 1;
                        break;
                    }
                case PlayerOutput.Movement_e.kMovement_Up:
                    {
                        wantedPos.y -= 1;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            // Handle wrap-around cases
            wantedPos = HandleWrapAround(wantedPos);
            if (IsValidWorldPos(wantedPos) && mGrid[wantedPos.x, wantedPos.y] == null)
            {
                mGrid[wantedPos.x, wantedPos.y] = mGrid[_unitCurPos.x, _unitCurPos.y];
                mGrid[_unitCurPos.x, _unitCurPos.y] = null;
                mGrid[wantedPos.x, wantedPos.y].unit.Pos.x = wantedPos.x;
                mGrid[wantedPos.x, wantedPos.y].unit.Pos.y = wantedPos.y;
                return true;
            }
            return false;
        }

		public ISimulationOutput ProduceSimulationOutput()
		{
			UpdateTurn();

			SimulationOutput toReturn = new SimulationOutput();
            toReturn.gridWidth  = muiGridWidth;
            toReturn.gridHeight = muiGridHeight;
            toReturn.turnTime   = mFrequency;

            Int32 numPlayers = mPlayerMap.Count();
            toReturn.players = new SimulationOutput.PlayerData[numPlayers];

            int curPlayerIdx = 0;
            foreach (KeyValuePair<IPlayer, PlayerData> curPair in mPlayerMap)
            {
                IPlayer    curPlayer = curPair.Key;
                PlayerData curData = curPair.Value;

                SimulationOutput.PlayerData outData = new SimulationOutput.PlayerData();
                outData.player = curPlayer;
                outData.LastTurnTime = curData.LastTurnTime;

                Int32 numUnits = curData.units.Count();
                outData.units = new Unit[numUnits];

                int curUnitIdx = 0;
                foreach (Unit curUnit in curData.units.Values)
                {
                    outData.units[curUnitIdx] = curUnit.CopyDeep();
                    ++curUnitIdx;
                }

                toReturn.players[curPlayerIdx] = outData;

                ++curPlayerIdx;
            }

			return toReturn;
		}

		public void ApplyRendererFeedback(IRendererFeedback _rendererFeedback)
		{
			RendererFeedback feedback = _rendererFeedback as RendererFeedback;
			if (feedback == null)
				return;

			// Just check if the renderer wants us to quit
			if (feedback.ShouldQuit)
			{
                mIsGameFinished = true;
				mbShouldRestart = feedback.ShouldRestart;
			}
		}

		public void OnGameEnd()
		{
			// Nothing to do here for now
		}

		public bool IsGameFinished()
		{
            return mIsGameFinished;
		}

		public bool ShouldRestart()
		{
			return mbShouldRestart;
		}

        public TimeSpan GetFrequency()
        {
            return mFrequency;
        }
		#endregion


		#region Private Interface

        class PlayerUnit
        {
            public PlayerData owner;
            public Unit unit;
        }

        class PlayerData
        {
            public Dictionary<UInt32, Unit> units = new Dictionary<UInt32, Unit>();
            public TimeSpan LastTurnTime { get; set; }
        }

        class UnitPattern
        {
            public Pos2D[] data = new Pos2D[0];
        }

        bool mIsGameFinished = false;
        bool mbShouldRestart = false;
        Random mRNG = new Random();

        // Parameters
        const string mParametersFile = "Parameters.xml";
        UInt32   muiGridWidth      = 1024;
        UInt32   muiGridHeight     = 768;
        UInt32   muiUnitsPerPlayer = 1024;
        TimeSpan mFrequency = new TimeSpan(0, 0, 0, 0, 33);	// 33 ms per frame
        bool     mbWrapAround = true;

        // Game data
        Dictionary<IPlayer, PlayerData> mPlayerMap = new Dictionary<IPlayer, PlayerData>();
        PlayerUnit[,] mGrid = null;
        List<UnitPattern> mPatternList = new List<UnitPattern>();
		
		void Init()
		{
            mPlayerMap.Clear();
            mPatternList.Clear();

            mIsGameFinished = false;
            mbShouldRestart = false;

            InitParameters();
            InitPlayerData();
            InitGrid();
		}

        void InitParameters()
        {
            if (!File.Exists(mParametersFile))
                return;

            try
            {
                XElement root = XElement.Load(mParametersFile);

                XElement gridWidthNode = root.Descendants("GridWidth").FirstOrDefault();
                UInt32.TryParse(gridWidthNode.Value, out muiGridWidth);

                XElement gridHeightNode = root.Descendants("GridHeight").FirstOrDefault();
                UInt32.TryParse(gridHeightNode.Value, out muiGridHeight);

                XElement unitsPerPlayerNode = root.Descendants("UnitsPerPlayer").FirstOrDefault();
                UInt32.TryParse(unitsPerPlayerNode.Value, out muiUnitsPerPlayer);

                int msFreq = 0;
                XElement timeSpanNode = root.Descendants("TimeSpan").FirstOrDefault();
                if (int.TryParse(timeSpanNode.Value, out msFreq))
                    mFrequency = new TimeSpan(0, 0, 0, 0, msFreq);

                int iWrapAround = 0;
                XElement wrapAroundNode = root.Descendants("WrapAround").FirstOrDefault();
                if (int.TryParse(wrapAroundNode.Value, out iWrapAround))
                    mbWrapAround = iWrapAround != 0;

                // Load all patterns
                XElement patternListNode = root.Descendants("PatternList").FirstOrDefault();
                for (XNode patternNode = patternListNode.FirstNode; patternNode != null; patternNode = patternNode.NextNode)
                {
                    // Ignore comments
                    XElement patternElem = patternNode as XElement;
                    if (patternElem == null)
                        continue;

                    string strPattern = patternElem.Value;
                    string[] strPosList = strPattern.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                    int numPos = strPosList.GetLength(0);
                    UnitPattern newPattern = new UnitPattern();
                    newPattern.data = new Pos2D[numPos];
                    mPatternList.Add(newPattern);

                    // Load all positions for this pattern
                    for (int curPosIdx = 0; curPosIdx < numPos; ++curPosIdx)
                    {
                        string   strCurPos    = strPosList[curPosIdx];
                        string[] strPosValues = strCurPos.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        int.TryParse(strPosValues[0], out newPattern.data[curPosIdx].x);
                        int.TryParse(strPosValues[1], out newPattern.data[curPosIdx].y);
                    }
                }                
            }
            catch (Exception e)
            {
                Debug.WriteLine("Failed to load XML param file: " + e.Message);
            }
        }

        void InitPlayerData()
        {
            foreach (IPlayer curPlayer in PlayerList)
                mPlayerMap[curPlayer] = new PlayerData();
        }

        public void Shuffle<T>(IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = mRNG.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        void InitGrid()
        {
            // Start with an empty grid
            mGrid = new PlayerUnit[muiGridWidth, muiGridHeight];
            
            // Spawn each player's units in a square
            int iUnitsSquareSide = (int)Math.Sqrt(muiUnitsPerPlayer);
            int iHalfSquareSide  = iUnitsSquareSide / 2;

            // Find center
            UInt32 uiCenterX = muiGridWidth / 2;
            UInt32 uiCenterY = muiGridHeight / 2;

            float  fDistToCenter  = Math.Min(uiCenterX, uiCenterY) * 0.75f;
            UInt32 uiUnitUniqueID = 0;
            
            // Spawn players' units in a circle around the middle
            int   numPlayers      = mPlayerMap.Count;
            float fAnglePerPlayer = (float)(2.0 * Math.PI / numPlayers);
            float fCurrentAngle   = 0.0f;

            // Shuffle player ordering
            List<PlayerData> playerDataList = new List<PlayerData>(mPlayerMap.Values);
            Shuffle(playerDataList);

            foreach (PlayerData curData in playerDataList)
            {
                // Find where to spawn the player
                float fDeltaX = (float)Math.Sin(fCurrentAngle) * fDistToCenter;
                float fDeltaY = (float)Math.Cos(fCurrentAngle) * fDistToCenter;

                float fSpawnPointX = uiCenterX + fDeltaX;
                float fSpawnPointY = uiCenterY + fDeltaY;

                int iLeftCoord = (int)fSpawnPointX - iHalfSquareSide;
                int iTopCoord  = (int)fSpawnPointY - iHalfSquareSide;

                // Validate that we won't spawn units out of the grid
                Debug.Assert(iLeftCoord >= 0);
                Debug.Assert(iLeftCoord < muiGridWidth - iUnitsSquareSide);
                Debug.Assert(iTopCoord >= 0);
                Debug.Assert(iTopCoord < muiGridHeight - iUnitsSquareSide);

                // Spawn all units
                for (int curY = 0; curY < iUnitsSquareSide; ++curY)
                {
                    for (int curX = 0; curX < iUnitsSquareSide; ++curX)
                    {
                        int curCoordX = curX + iLeftCoord;
                        int curCoordY = curY + iTopCoord;

                        Unit newUnit = new Unit();
                        newUnit.UniqueID = uiUnitUniqueID;
                        newUnit.Pos.x = curCoordX;
                        newUnit.Pos.y = curCoordY;

                        // Give the unit to the player
                        curData.units[newUnit.UniqueID] = newUnit;

                        // Place the unit on the grid
                        PlayerUnit newPlayerUnit = new PlayerUnit();
                        newPlayerUnit.unit  = newUnit;
                        newPlayerUnit.owner = curData;

                        Debug.Assert(mGrid[curCoordX, curCoordY] == null);
                        mGrid[curCoordX, curCoordY] = newPlayerUnit;

                        ++uiUnitUniqueID;
                    }
                }

                // Rotate for next player
                fCurrentAngle += fAnglePerPlayer;
            }
        }

		void UpdateTurn()
		{
            List<int> xList = new List<int>();
            for (int xCoord = 0; xCoord < muiGridWidth; ++xCoord)
                xList.Add(xCoord);
            List<int> yList = new List<int>();
            for (int yCoord = 0; yCoord < muiGridHeight; ++yCoord)
                yList.Add(yCoord);

            Shuffle(xList);
            foreach(int xCoord in xList)
            {
                Shuffle(yList);
                foreach (int yCoord in yList)
                {
                    if (mGrid[xCoord, yCoord] != null)
                    {
                        PlayerData newOwner = ValidatePattern(mGrid[xCoord, yCoord]);
                        if (newOwner != null)
                        {
                            mGrid[xCoord, yCoord].owner.units.Remove(mGrid[xCoord, yCoord].unit.UniqueID);
                            mGrid[xCoord, yCoord].owner = newOwner;
                            mGrid[xCoord, yCoord].owner.units.Add(mGrid[xCoord, yCoord].unit.UniqueID, mGrid[xCoord, yCoord].unit);
                        }
                    }
                }
            }

            // Count number of players with units > 0. If <= 1 player, end the game.
            int numAlivePlayers = 0;
            foreach (PlayerData curData in mPlayerMap.Values)
            {
                if (curData.units.Count > 0)
                {
                    ++numAlivePlayers;
                    if (numAlivePlayers > 1)
                        break;
                }
            }

            if (numAlivePlayers <= 1)
                mIsGameFinished = true;
		}

        PlayerData ValidatePattern(PlayerUnit _unitToAbsorb)
        {
            PlayerData curEnemy = null;

            // Loop on each pattern
            foreach (UnitPattern curPattern in mPatternList)
            {
                curEnemy = null;

                // Loop on each position of this pattern
                foreach (Pos2D paternPos in curPattern.data)
                {
                    Pos2D worldPos = new Pos2D(paternPos.x + _unitToAbsorb.unit.Pos.x, paternPos.y + _unitToAbsorb.unit.Pos.y);

                    // Handle wrap around
                    worldPos = HandleWrapAround(worldPos);

                    // Empty cell, or invalid pos (out of grid)
                    if (!IsValidWorldPos(worldPos) || mGrid[worldPos.x, worldPos.y] == null)
                    {
                        curEnemy = null;
                        break;
                    }

                    PlayerUnit enemyUnit = mGrid[worldPos.x, worldPos.y];

                    if (curEnemy == null)
                    {
                        // One of our own unit inside the pattern, ignore.
                        if (enemyUnit.owner == _unitToAbsorb.owner)
                        {
                            curEnemy = null;
                            break;
                        }

                        curEnemy = enemyUnit.owner;
                    }
                    else if (enemyUnit.owner != curEnemy)
                    {
                        // Various enemies on the pattern, ignore. Must be from the same enemy.
                        curEnemy = null;
                        break;
                    }
                }

                // Found a valid pattern
                if (curEnemy != null)
                    break;
            }

            return curEnemy;
        }

        bool IsValidWorldPos(Pos2D _worldPos)
        {
            return _worldPos.x >= 0 && _worldPos.x < muiGridWidth && _worldPos.y >= 0 && _worldPos.y < muiGridHeight;
        }
		
		#endregion
	}
}

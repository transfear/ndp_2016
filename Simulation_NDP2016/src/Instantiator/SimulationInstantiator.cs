﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

using FrameworkInterface.Player;
using FrameworkInterface.Renderer;
using FrameworkInterface.Simulation;

namespace Simulation_NDP2016.Instantiator
{
    public class SimulationInstantiator : ISimulationInstantiator
    {
        public override IRenderer InstantiateRenderer(List<Type> _rendererTypeList)
        {
            // In this example simulation, always use the first type for renderer
            IRenderer toReturn = null;
            if (_rendererTypeList.Count > 0)
            {
                Type firstType = _rendererTypeList.First();
                try
                {
                    toReturn = (IRenderer)Activator.CreateInstance(firstType);
                    Console.WriteLine("Using class " + firstType.Name + " as renderer.");
                }
                catch (System.Exception exCtor)
                {
                    Console.WriteLine("Tried to use class " + firstType.Name + " as renderer, but following error occured: " + exCtor.Message);
                }
            }
            return toReturn;
        }

        public override List<IPlayer> InstantiatePlayers(List<Type> _playerTypeList)
        {
            // In this example simulation, always instantiate one player of each type
            List<IPlayer> playerList = new List<IPlayer>();
            foreach (Type curPlayerType in _playerTypeList)
            {
                try
                {
                    IPlayer newPlayer = (IPlayer)Activator.CreateInstance(curPlayerType);
                    Console.WriteLine("Instantiating player class " + curPlayerType.Name + ".");

                    playerList.Add(newPlayer);
                }
                catch (System.Exception exCtor)
                {
                    Console.WriteLine("Tried to instantiate player class " + curPlayerType.Name + ", but following error occurred: " + exCtor.Message);
                }
            }

            return playerList;
        }

        public override ISimulation InstantiateSimulation(List<Type> _simulationTypeList)
        {
            ISimulation toReturn = null;
            Type toInstantiate = _simulationTypeList.FirstOrDefault();

            if (toInstantiate != null)
            {
                try
                {
                    toReturn = (ISimulation)Activator.CreateInstance(toInstantiate);
                    Console.WriteLine("Using class " + toInstantiate.Name + " as simulation.");
                }
                catch (System.Exception exCtor)
                {
                    Console.WriteLine("Tried to use class " + toInstantiate.Name + " as simulation, but following error occured: " + exCtor.Message);
                }
            }

            return toReturn;
        }
    }
}

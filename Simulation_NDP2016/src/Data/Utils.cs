﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simulation_NDP2016.Data
{
    public struct Pos2D
    {
        public int x;
        public int y;

        public Pos2D(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public void Add(Pos2D _toAdd)
        {
            x += _toAdd.x;
            y += _toAdd.y;
        }

        public void Subtract(Pos2D _toSub)
        {
            x -= _toSub.x;
            y -= _toSub.y;
        }

        public void Multiply(int _multiplier)
        {
            x *= _multiplier;
            y *= _multiplier;
        }

        public void Divide(int _divider)
        {
            x /= _divider;
            y /= _divider;
        }
    }

    public class Unit
    {
        public UInt32 UniqueID;
        public Pos2D Pos;

        public Unit CopyDeep()
        {
            Unit copy = new Unit();
            copy.UniqueID = UniqueID;
            copy.Pos = Pos;
            return copy;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;

namespace Simulation_NDP2016.Data
{
    /// <summary>
    /// Use this class to move your units around the grid.
    /// The top left corner of the grid has the coordinates (0, 0)
    /// and the bottom right has the coordinates (width-1, height-1).
    /// </summary>
	public class PlayerOutput : IPlayerOutput
	{
		public enum Movement_e
		{
            /// <summary> Move up. Reduce Y coordinate by 1 </summary>
			kMovement_Up = 0,

            /// <summary> Move right. Increase X coordinate by 1 </summary>
			kMovement_Right,

            /// <summary> Move down. Increase Y coordinate by 1 </summary>
			kMovement_Down,

            /// <summary> Move left. Reduce Y coordinate by 1 </summary>
			kMovement_Left,

            /// <summary> Don't move. </summary>
            kMovement_None,

            kMovement_Count
		};

        /// <summary> Each instance of this struct will move a unit </summary>
        public struct UnitMovement
        {
            /// <summary> Unique ID of the unit to move </summary>
            public UInt32     unitID;

            /// <summary> Direction in which to move the unit </summary>
            public Movement_e move;
        }

        public UnitMovement[] movement;

		// From IPlayerOutput interface
		public override IPlayerOutput CopyDeep()
		{
            PlayerOutput copy = new PlayerOutput();

            // Copy parent members
            CopyDeepInternal(copy);

            if (movement != null)
            {
                Int32 numUnits = movement.GetLength(0);
                copy.movement = new UnitMovement[numUnits];
                Array.Copy(movement, copy.movement, numUnits);
            }

			return copy;
		}
	}
}

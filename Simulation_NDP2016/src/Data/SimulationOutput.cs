﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;
using FrameworkInterface.Simulation;

namespace Simulation_NDP2016.Data
{
    public class SimulationOutput : ISimulationOutput
    {
        public class PlayerData
        {
            public IPlayer  player;
            public Unit[]   units;
            public TimeSpan LastTurnTime { get; set; }

            public PlayerData CopyDeep()
            {
                PlayerData copy = new PlayerData();
                copy.player = player;
                copy.LastTurnTime = LastTurnTime;

                Int32 numUnits = units.GetLength(0);
                copy.units = new Unit[numUnits];
                Array.Copy(units, copy.units, numUnits);

                return copy;
            }
        };

		public PlayerData[] players;

        public UInt32   gridWidth;
        public UInt32   gridHeight;
        public TimeSpan turnTime;

		// From ISimulationOutput interface
		public ISimulationOutput CopyDeep()
		{
			SimulationOutput copy = new SimulationOutput();

			Int32 iNumPlayers = players.GetLength(0);
			copy.players = new PlayerData[iNumPlayers];
			for (Int32 iCurPlayer = 0; iCurPlayer < iNumPlayers; ++iCurPlayer)
				copy.players[iCurPlayer] = players[iCurPlayer].CopyDeep();

            copy.gridWidth  = gridWidth;
            copy.gridHeight = gridHeight;
            copy.turnTime   = turnTime;

			return copy;
		}
	}
}

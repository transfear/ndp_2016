﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Renderer;

namespace Simulation_NDP2016.Data
{
	public class RendererFeedback : IRendererFeedback
	{
		public bool ShouldQuit    { get; set; }
		public bool ShouldRestart { get; set; }

		// From IRendererFeedback interface
		public IRendererFeedback CopyDeep()
		{
			RendererFeedback copy = new RendererFeedback();
			copy.ShouldQuit    = ShouldQuit;
			copy.ShouldRestart = ShouldRestart;

			return copy;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;

namespace Simulation_NDP2016.Data
{
	public class PlayerInput : IPlayerInput
	{
        /// <summary> Struct representing data related to a player </summary>
		public class PlayerData
		{
            /// <summary> Unique player index </summary>
			public UInt32 playerIndex;

            /// <summary> Array of units controlled by this player </summary>
            public Unit[] units;

            public PlayerData CopyDeep()
            {
                PlayerData copy = new PlayerData();

                copy.playerIndex = this.playerIndex;

                Int32 numUnits = units.GetLength(0);
                copy.units = new Unit[numUnits];
                for (int curUnitIdx = 0; curUnitIdx < numUnits; ++curUnitIdx)
                    copy.units[curUnitIdx] = units[curUnitIdx].CopyDeep();

                return copy;
            }
		};

        /// <summary> Your own data </summary>
        public PlayerData MyData;

        /// <summary> Array of enemies' data </summary>
        public PlayerData[] EnemyData;

        /// <summary> Width of the grid </summary>
        public UInt32 GridWidth;

        /// <summary> Height of the grid </summary>
        public UInt32 GridHeight;

		public IPlayerInput CopyDeep()
		{
			PlayerInput copy = new PlayerInput();

            copy.MyData     = MyData.CopyDeep();
            copy.GridWidth = GridWidth;
            copy.GridHeight = GridHeight;

            Int32 numEnemies = EnemyData.GetLength(0);
            copy.EnemyData = new PlayerData[numEnemies];
            Array.Copy(EnemyData, copy.EnemyData, numEnemies);

			return copy;
		}
	}
}

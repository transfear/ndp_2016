﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

using FrameworkInterface.Simulation;
using FrameworkInterface.Player;
using FrameworkInterface.Renderer;

namespace Framework
{
	class RealTimeUpdater : IUpdater
	{
		FrameworkInterface.Simulation.RealTime.ISimulation mSimulation = null;
		IRenderer mRenderer = null;
		ICollection<IPlayer> mPlayers = null;

		AutoResetEvent[] mPlayerEvents = null;
		IPlayerInput[] mPlayerInputs = null;

		Dictionary<IPlayer, IPlayerOutput> mPlayerOutputs = new Dictionary<IPlayer, IPlayerOutput>();

		public void Update(ISimulation _simulation, IRenderer _renderer, ICollection<IPlayer> _playerCollection)
		{
			mSimulation = _simulation as FrameworkInterface.Simulation.RealTime.ISimulation;
			mRenderer = _renderer;
			mPlayers = _playerCollection;

			// Initialize player events
			Int32 iNumPlayers = mPlayers.Count;
			mPlayerEvents = new AutoResetEvent[iNumPlayers];
			for (int i = 0; i < iNumPlayers; ++i)
				mPlayerEvents[i] = new AutoResetEvent(false);

			mPlayerInputs = new IPlayerInput[iNumPlayers];

			// Start everyone's threaded update
			Thread[] playerThreads = new Thread[iNumPlayers];
			Int32 iCurPlayer = 0;
			foreach (IPlayer curPlayer in mPlayers)
			{
				Thread playerThread = new Thread(
					(o) =>
					{
						IPlayer capturedPlayer = ((object[])o)[0] as IPlayer;
						Int32 iPlayerIndex = (Int32)((object[])o)[1];
						ThreadedPlayerUpdate(capturedPlayer, iPlayerIndex);
					}
					);
				playerThread.Start(new object[] { curPlayer, iCurPlayer });

				playerThreads[iCurPlayer] = playerThread;
				++iCurPlayer;
			}


			// Simulation loop
			Stopwatch curTime = new Stopwatch();
			bool bGameFinished = mSimulation.IsGameFinished();
			while (!bGameFinished)
			{
				// Prepare inputs
				ICollection<IPlayerInput> playerInputs = mSimulation.PrepareInputs(new List<IPlayer>(mPlayers));
				Debug.Assert(playerInputs.Count == mPlayers.Count);

				iCurPlayer = 0;
				foreach (IPlayerInput curPlayerInput in playerInputs)
				{
					lock (mPlayerInputs)
					{
						mPlayerInputs[iCurPlayer] = curPlayerInput;
					}
					++iCurPlayer;
				}

				// Release all players
				foreach (AutoResetEvent playerEvent in mPlayerEvents)
					playerEvent.Set();

				// Sync frequency if main simulation is too fast
				TimeSpan simulationFrequency = mSimulation.GetUpdateFrequency();
				TimeSpan elapsed = curTime.Elapsed;
				if (elapsed < simulationFrequency)
				{
					TimeSpan sleepTime = simulationFrequency - elapsed;
					Thread.Sleep(sleepTime);
				}
				curTime.Restart();

				// Grab outputs as they come in
				List<Tuple<IPlayer, IPlayerOutput>> localOutputList = null;
				lock (mPlayerOutputs)
				{
					localOutputList = new List<Tuple<IPlayer, IPlayerOutput>>();
					foreach (KeyValuePair<IPlayer, IPlayerOutput> curPair in mPlayerOutputs)
						localOutputList.Add(new Tuple<IPlayer, IPlayerOutput>(curPair.Key, curPair.Value));

					mPlayerOutputs.Clear();
				}

				// We should never update a player more than once per turn
				Debug.Assert(localOutputList.Count() <= mPlayers.Count());

				// Apply player outputs to the simulation
				ISimulationOutput simOutput = mSimulation.ApplyUpdate(localOutputList);
				if (simOutput != null)
					simOutput = simOutput.CopyDeep();

				// Notify renderer
				IRendererFeedback feedback = mRenderer.Update(simOutput);
				if (feedback != null)
					feedback = feedback.CopyDeep();
				mSimulation.ApplyRendererFeedback(feedback);

				bGameFinished = mSimulation.IsGameFinished();
			}

			// Signal other players threads to finish, and join their thread
			foreach (AutoResetEvent playerEvent in mPlayerEvents)
				playerEvent.Set();
			foreach (Thread curPlayerThread in playerThreads)
				curPlayerThread.Join();
		}

		private void ThreadedPlayerUpdate(IPlayer _player, Int32 _iPlayerIdx)
		{
            Stopwatch sw = new Stopwatch();

			// Wait for everyone to be ready
			mPlayerEvents[_iPlayerIdx].WaitOne();
			bool bGameFinished = mSimulation.IsGameFinished();

			while (!bGameFinished)
			{
				// Grab input
				IPlayerInput curInput = null;
				lock (mPlayerInputs)
				{
					if (mPlayerInputs[_iPlayerIdx] != null)
						curInput = mPlayerInputs[_iPlayerIdx].CopyDeep();
				}

                sw.Restart();
				IPlayerOutput playerOutput = _player.Update(curInput);
                sw.Stop();
                playerOutput.TurnTime = sw.Elapsed;

				if (playerOutput != null)
					playerOutput = playerOutput.CopyDeep();

				// Save output
				lock (mPlayerOutputs)
				{
					if (mPlayerOutputs.ContainsKey(_player))
						mPlayerOutputs[_player] = playerOutput;
					else
						mPlayerOutputs.Add(_player, playerOutput);
				}

				// Wait for the next update to occur
				mPlayerEvents[_iPlayerIdx].WaitOne();
				bGameFinished = mSimulation.IsGameFinished();
			}
		}
	}
}

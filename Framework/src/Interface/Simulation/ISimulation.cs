﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;

namespace FrameworkInterface.Simulation
{
	public enum SimulationType_e
	{
		kSimulationType_LockedStepSequential,    // Turn-based simulation, players are updated one after the other
		kSimulationType_LockedStepParallel,      // Turn-based simulation, players are updated in parallel
		kSimulationType_RealTime                 // Realtime simulation
	}

	public abstract class ISimulation
	{
		// Determines how the framework updates the simulation and the players
		public abstract SimulationType_e GetSimulationType();

		// Called just before IPlayer.OnGameStart
		public abstract ICollection<IPlayerInput> OnGameStart_Begin(ICollection<IPlayer> _playerCollection);

		// Called just after IPlayer.OnGameStart, and just before IRenderer.OnGameStart
		public abstract ISimulationOutput OnGameStart_End(ICollection<IPlayerOutput> _playersResponse);

		// Called just before IPlayer.OnGameEnd
		public abstract ICollection<IPlayerInput> OnGameEnd_Begin(ICollection<IPlayer> _playerCollection);

		// Called just after IPlayer.OnGameStart, and just before IRenderer.OnGameStart
		public abstract ISimulationOutput OnGameEnd_End(ICollection<IPlayerOutput> _playersResponse);
		
		// Called after each renderer update, and just after IRenderer.OnGameStart and IRenderer.OnGameEnd
		public abstract void ApplyRendererFeedback(FrameworkInterface.Renderer.IRendererFeedback _rendererFeedback);

		// Called each turn (or on a fixed frequency when using kSimulationType_RealTime) to let the framework that the game should end
		public abstract bool IsGameFinished();

		// Called after the end of a game, let the framework know if it should start a new game
		public abstract bool ShouldRestart();
	}
}

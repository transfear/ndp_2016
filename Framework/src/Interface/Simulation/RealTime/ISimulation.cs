﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;

namespace FrameworkInterface.Simulation.RealTime
{
	public abstract class ISimulation : FrameworkInterface.Simulation.ISimulation
	{
		public override SimulationType_e GetSimulationType() { return SimulationType_e.kSimulationType_RealTime; }

		// Determines the frequency at which the simulation is ran
		public abstract TimeSpan GetUpdateFrequency();

		// Called at a fixed frequency. Implementer has to produce input for all players.
		public abstract ICollection<IPlayerInput> PrepareInputs(ICollection<IPlayer> _playerCollection);

		// Called at a fixed frequency. The collection holds every player data that has given an output since the last update.
		public abstract ISimulationOutput ApplyUpdate(ICollection<Tuple<IPlayer, IPlayerOutput>> _playerOutputs);
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrameworkInterface.Player
{
	public abstract class IPlayerOutput
	{
		public abstract IPlayerOutput CopyDeep();

		public TimeSpan TurnTime { get; set; }

        protected void CopyDeepInternal(IPlayerOutput _out)
        {
            _out.TurnTime = TurnTime;
        }
	}
}
